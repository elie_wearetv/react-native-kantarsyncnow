//
//
// A React Native Plugin that uses the Kantar SDK
//
// This plugin gets 3 optionnal params:
// - licence
// - identifierBits
// - timestampBits
//
// The plugin will return the timestamp every time it recognize timecode on sound (emitter).
//
// The Object returned is structured like this:
//   {
//     'data'    : An Object with data inside when it gets a timestamp or ID,
//     'error'   : A Boolean,
//     'message' : A String Debug Message,
//   }
//
//
//
//

'use strict';

var React = require('react-native');
var SyncNowManager = require('react-native').NativeModules.SyncNowManager;

var {
  View,
  NativeAppEventEmitter,
} = React;

var subscription;

var SyncNowComponent = React.createClass({
  propTypes: {
    licence: React.PropTypes.string,
    identifierBits: React.PropTypes.number,
    timestampBits: React.PropTypes.number,
    callback: React.PropTypes.func,
  },
  getDefaultProps: function() {
    return {
      // Demo Licence
      licence : "cAC+/2O/kZt6VRQsJHz42Eh8NiCueYTF1KjwuaZO1wgjnO5l+xlLr45g6NaT8//X66rdoc8wUPZ14vX+7JtJfj+TzaOHatsViRbgKV2KAoCDGMys81q/LMts8qxmJloGlB1IYmIa5cZm7fT2oNY9Z8bNQQBNQ3dDRkhLZENTWGVoWjFjeFFXQ05FSDY2bnhJL0oxckFoUXJhWkV3bXhtY0xrNFNqc2czcUI2RFphdDBVUT09AA==",
      identifierBits: 4,
      timestampBits: 8,
      callback: (response) => {
        // Default Callback if not passed
        if (response.error) {
          console.error(response.message);
        } else {
          console.log(response.message, response.data);
        }
      },
    };
  },
  componentDidMount: function() {
    SyncNowManager.initSyncNow( this.props.licence,
                                this.props.identifierBits,
                                this.props.timestampBits);
    // Suscribing for detection event with callback
    subscription = NativeAppEventEmitter.addListener( 'SyncNowEvent',
                                                      this.props.callback);
  },
  componentWillUnmount: function() {
    // Unsubscribing from the detection event
    subscription.remove();
  },
  render: function() {
    return (
      <View />
    );
  }
});

module.exports = SyncNowComponent;
