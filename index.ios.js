/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 */
'use strict';

var React = require('react-native');
var SyncNowComponent = require('./SyncNowComponent');
var {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  ScrollView,
} = React;

var ReactNativeKantar = React.createClass({
  getInitialState: function() {
    return {
      text: "HELLO\n",
    };
  },
  _dataToString: function(data) {
    var output = "";
    if (data) {
      for (var row in data) {
        if (data.hasOwnProperty(row)) {
          output += `"${row}": ${data[row]}\n`;
        }
      }
    }
    return output;
  },
  addLog: function(response) {
    var text = this.state.text;
    var output = ""
    if (response.error) {
      output += `[Error]`
    }
    if (response.message) {
      output += `${response.message}\n`
    }
    var dataString = this._dataToString(response.data);
    if (dataString.length) {
      output += `${dataString}\n`
    }
    if (output.length) {
      this.setState({
        text: `${output}\n` + text
      })
    }
  },
  render: function() {
    return (
      <View style={styles.mainView}>
        <Text style={styles.title}>REACT NATIVE KANTAR SYNCNOW</Text>
        <ScrollView style={styles.scrollView}>
          <Text style={styles.text}>{this.state.text}</Text>
          <SyncNowComponent callback={this.addLog} identifierBits={4} timestampBits={8} />
        </ScrollView>
      </View>
    );
  }
});

var styles = StyleSheet.create({
  scrollView: {
    flex: 1,
  },
  mainView: {
    flex: 1,
  },
  title: {
    fontSize: 18,
    margin: 20,
    marginTop: 40,
    fontWeight: 'bold',
  },
  text: {
    flex: 1,
    padding: 20,
  },
});



AppRegistry.registerComponent('ReactNativeKantar', () => ReactNativeKantar);
