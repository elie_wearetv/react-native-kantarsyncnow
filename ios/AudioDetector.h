/**
 * Copyright (c) 2014 Kantar Media. All rights reserved.
 *
 * This source code and any compilation or derivative thereof is the proprietary
 * information of Kantar Media. and is confidential in nature.
 *
 * Under no circumstances is this software to be combined with any Open Source
 * Software in any way or placed under an Open Source License of any type without
 * the express written permission of Kantar Media.
 *
 * Platform   : ios
 * Created    : 17/01/2014
 * Author     : lraulet
 *
 * Stream_Reader.h
 */

#include "AwmSyncDetector.h"
#include "AudioCaptureConfiguration.h"

@protocol AudioDetectorDelegate <NSObject>
- (void) sendMessage:(NSString *)text;
- (void) sendInfo:(NSObject *)data;
@end

// Class for configuration
@interface AudioDetectorConfig: NSObject

@property (nonatomic) NSString *license;
@property (nonatomic) int payloadIdNumberOfBits;
@property (nonatomic) int timeStampNumberOfBits;
@property (nonatomic) BOOL timeStampLoop;
@property (nonatomic) BOOL log;

@end

@interface AudioDetector : NSObject <AwmSyncDetectorDelegate>
{
  AwmSyncDetector                 *detector;
  NSFileHandle                    *fileHandler;
  AudioDetectorConfig              *detectorConfiguration;
  struct AudioCaptureConfiguration       audioCaptureConfiguration;
}

@property (readonly, nonatomic) BOOL	isRunning;
@property (nonatomic,strong) id delegate;


- (id) initWithUI:(struct AudioCaptureConfiguration) audioCaptureConfig audioDetectionConfig:(AudioDetectorConfig*) audioDetectionConfig;
- (int) readBuffer:(void *) buffer WithBufferSize:(NSInteger) size;
- (void) stopReader;
- (BOOL) startReader:(BOOL) record;
+ (NSString *) getVersion;
- (BOOL) getWmkPresence;

@end