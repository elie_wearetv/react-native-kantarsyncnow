/**
 * Copyright (c) 2014 Kantar Media. All rights reserved.
 *
 * This source code and any compilation or derivative thereof is the proprietary
 * information of Kantar Media. and is confidential in nature.
 *
 * Under no circumstances is this software to be combined with any Open Source
 * Software in any way or placed under an Open Source License of any type without
 * the express written permission of Kantar Media.
 *
 * Platform   : ios
 * Created    : 17/09/2014
 * Author     : lraulet
 *
 * AudioCaptureConfiguration.h
 */

#ifndef SyncNowSample_AudioCaptureConfiguration_h
#define SyncNowSample_AudioCaptureConfiguration_h

struct AudioCaptureConfiguration
{
    Float64 sampleRate;
    int bitsPerChannel;
    int bufferSize;
    int numChannel;
} ;


#endif
