/**
 * Copyright (c) 2014 Kantar Media. All rights reserved.
 *
 * This source code and any compilation or derivative thereof is the proprietary
 * information of Kantar Media. and is confidential in nature.
 *
 * Under no circumstances is this software to be combined with any Open Source
 * Software in any way or placed under an Open Source License of any type without
 * the express written permission of Kantar Media.
 *
 * Platform   : ios
 * Created    : 17/01/2014
 * Author     : lraulet
 *
 * AudioDetector.mm
 */

#import "AudioDetector.h"
#import "SyncNowManager.h"


#pragma mark -
#pragma mark AudioDetector class

@implementation AudioDetectorConfig
@synthesize license;
@synthesize payloadIdNumberOfBits;
@synthesize timeStampNumberOfBits;
@synthesize timeStampLoop;
@synthesize log;

@end

@implementation AudioDetector
@synthesize isRunning;

- (id) initWithUI:(struct AudioCaptureConfiguration) audioCaptureConfig audioDetectionConfig:(AudioDetectorConfig*) audioDetectionConfig;
{
  if (self = [super init])
  {
    isRunning = NO;
    detectorConfiguration = audioDetectionConfig;
    audioCaptureConfiguration.sampleRate = audioCaptureConfig.sampleRate;
    audioCaptureConfiguration.bitsPerChannel = audioCaptureConfig.bitsPerChannel;
    audioCaptureConfiguration.bufferSize = audioCaptureConfig.bufferSize;
    audioCaptureConfiguration.numChannel = audioCaptureConfig.numChannel;
  }
  return self;
}

-(NSString *) getFilePath:(NSString *) name withExtension:(NSString *) ext
{
  NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
  NSString *documentsDirectory = [paths objectAtIndex:0];
  int inc = 1;
  NSString *filePath = [NSString stringWithFormat:@"%@/%@%d%@", documentsDirectory, name, inc, ext];
  while([[NSFileManager defaultManager] fileExistsAtPath:filePath])
  {
    inc++;
    filePath = [NSString stringWithFormat:@"%@/%@%d%@",
                documentsDirectory, name, inc, ext];
    
  }
  return filePath;
}

-(NSString *) getFileName:(NSString *) path
{
  NSArray *pathComponents = [path pathComponents];
  return [pathComponents objectAtIndex:([pathComponents count] - 1)];
}

- (void)dealloc
{
  if (self.isRunning)
    [AwmSyncDetectorFactory destroy:detector];
}

+ (NSString *) getVersion
{
  return [AwmSyncDetector getVersion];
}

- (BOOL) getWmkPresence
{
  return [detector getWatermarkPresence];
}

- (BOOL) startReader:(BOOL) record
{
  // AwSyncDetector creation via factory
  if ((detector = [AwmSyncDetectorFactory createAwmSyncDetector]) != nil)
  {
    SdkDetectorType detectorType = DETECTOR_TYPE_ERROR;
    NSLog(@"***********************\n* Start Stream Reader *\n***********************\n\n\n");
    
    //Set delegate just after AwSyncDetector creation
    detector.delegate = self;
    
    //Set license
    detectorType = [detector setLicense:detectorConfiguration.license];
    if ( DETECTOR_TYPE_ERROR == detectorType)
    {
      [self.delegate performSelectorOnMainThread: @selector(sendMessage:) withObject:@"Error during license setting\n"  waitUntilDone:NO];
      NSLog(@"Error during license setting\n");
      return FALSE;
    }
    //Activate recording feature
    if (record)
    {
      NSString *recordFilePath = [self getFilePath:@"SyncNow_" withExtension:@".wav"];
      NSString *recordFileName = [self getFileName:recordFilePath];
      if (![detector setRecorderFilename:recordFilePath])
      {
        NSLog(@"Error during record option activation\n");
        return FALSE;
      }
      [self.delegate performSelectorOnMainThread: @selector(sendMessage:) withObject:[NSString stringWithFormat:@"Record: %@\n", recordFileName]  waitUntilDone:NO];
    }
    //Activate log feature
    if (detectorConfiguration.log)
    {
      NSString *logFilePath = [self getFilePath:@"SyncNow_" withExtension:@".txt"];
      NSString *logFileName = [self getFileName:logFilePath];
      if (![detector setLogFilename:logFilePath])
      {
        NSLog(@"Error during log option activation\n");
        return FALSE;
      }
      [self.delegate performSelectorOnMainThread: @selector(sendMessage:) withObject:[NSString stringWithFormat:@"Log: %@\n", logFileName]  waitUntilDone:NO];
    }
    //Set audio parameters
    //if ( ![detector setAudioParameters:recorder->mFs numBitsPerchannel:BITSPERCHANNEL numChannels:recorder->mNumChannel buffLength:BUFF_SIZE])
    if ( ![detector setAudioParameters:audioCaptureConfiguration.sampleRate numBitsPerchannel:kBitsPerChannel numChannels:audioCaptureConfiguration.numChannel buffLength:kBufferSize])
    {
      
      NSLog(@"Error during audio format parameters initialization\n");
      return FALSE;
    }
    //Set detector parameters
    if (DETECTOR_TYPE_SYNCNOW == detectorType)
    {
      DetectorParameters *param = [[DetectorParameters alloc] init];
      param.mode = MODE_LIVE;
      param.numIdentifierBits = detectorConfiguration.payloadIdNumberOfBits;
      param.numTimeStampBits = detectorConfiguration.timeStampNumberOfBits;
      param.timestampLoop = detectorConfiguration.timeStampLoop;
      if (![detector setDetectorParameters:param])
      {
        NSLog(@"Error during syncnow detector parameters initialization\n");
        return FALSE;
      }
    }
    if (DETECTOR_TYPE_SNAP == detectorType)
    {
      SnapDetectorParameters *param = [[SnapDetectorParameters alloc] init];
      param.mode = MODE_LIVE;
      param.timestampLoop = detectorConfiguration.timeStampLoop;
      if (![detector setSnapDetectorParameters:param])
      {
        NSLog(@"Error during snap detector parameters initialization\n");
        return FALSE;
      }
    }
    //Init detector with all parameters and start detection
    if (![detector initialize])
    {
      NSLog(@"Error in detector initialization\n");
      return FALSE;
    }
  }
  else
  {
    [self.delegate performSelectorOnMainThread: @selector(sendMessage:) withObject:@"Detector not initialized, armv6 not supported\n"  waitUntilDone:NO];
  }
  isRunning=YES;
  return TRUE;
}

- (void) stopReader
{
  isRunning=NO;
  if (detector != nil)
  {
    NSLog(@"*********************\n* End Detector *\n*********************\n\n\n");
    [AwmSyncDetectorFactory destroy:detector];
    detector = nil;
    [fileHandler closeFile];
  }
}

- (int) readBuffer:(void *) buffer WithBufferSize:(NSInteger) size
{
  if (detector)
    if(![detector pushAudioBuffer:(const char *) buffer withBuffLength:size])
      NSLog(@"Error in pushAudioBuffer withSize");
  return 0;
}

#pragma mark -
#pragma mark AwmSyncDetector delegate methods

- (void) onPayload:(PayloadEvent *) event
{
  @autoreleasepool
  {
    NSString *header = @"[OnPayload] ";
    if (event.payloadType == TYPE_IDENTIFIED)
    {
      if ((event.contentID != -1) && (event.timeStamp == -1))
      {
        [self.delegate performSelectorOnMainThread: @selector(sendInfo:)
                                          withObject:@{ @"header": header,
                                                        @"ID": [NSString stringWithFormat:@"%lld", event.contentID],
                                                        @"confidence": [NSString stringWithFormat:@"%.2f", event.confidence],
                                                        }
                                          waitUntilDone:NO];
      }
      if ((event.timeStamp != -1) && (event.contentID == -1))
      {
        UtcAbsoluteDateAndTime *utcAbsoluteDateAndTime = [[UtcAbsoluteDateAndTime alloc] init];
        [detector translateIntoAbsoluteDateAndTime:event.timeStamp absoluteDateAndTime:&utcAbsoluteDateAndTime];
        NSString *s_utcAbsoluteDateAndTime = [NSString stringWithFormat:@"UTC %02d-%02d-%02d %02d:%02d:%02d", (int)utcAbsoluteDateAndTime.year,  (int)utcAbsoluteDateAndTime.month,  (int)utcAbsoluteDateAndTime.day,  (int)utcAbsoluteDateAndTime.hour,  (int)utcAbsoluteDateAndTime.minute,  (int)utcAbsoluteDateAndTime.second];

        [self.delegate performSelectorOnMainThread: @selector(sendInfo:)
                                        withObject:@{ @"header": header,
                                                      @"timestamp": [NSString stringWithFormat:@"%.3f", event.timeStamp],
                                                      @"utc_time": s_utcAbsoluteDateAndTime,
                                                      @"confidence": [NSString stringWithFormat:@"%.2f", event.confidence],
                                                      }
                                     waitUntilDone:NO];
      }
    }
    else if (event.payloadType == TYPE_NOT_IDENTIFIED)
    {
      [self.delegate performSelectorOnMainThread: @selector(sendMessage:) withObject:[NSString stringWithFormat:@"%@ Content not marked\n", header]  waitUntilDone:NO];
    }
    else if (event.payloadType == TYPE_MARKED_BUT_NOT_IDENTIFIED)
    {
      [self.delegate performSelectorOnMainThread: @selector(sendMessage:) withObject:[NSString stringWithFormat:@"%@ Content marked but not identified\n", header]  waitUntilDone:NO];
    }
  }
}

- (void) onAlarm:(AlarmEvent *) event
{
  @autoreleasepool
  {
    NSString *type;
    switch (event.type ) {
      case TYPE_INFO:
      default:
        type = @"INFO";
        break;
      case TYPE_ERROR:
        type = @"ERROR";
        break;
      case TYPE_WARNING:
        type = @"WARNING";
        break;
    }
    NSString *header = [NSString stringWithFormat:@"[OnAlarm]: %@", type];
    if ( INFO_CONFIDENCE_VALUE != event.code )
    {
      [self.delegate performSelectorOnMainThread: @selector(sendMessage:) withObject:[NSString stringWithFormat:@"%@ %@\n", header, event.message]  waitUntilDone:NO];
    }
  }
}

- (void) onDebug:(NSString *) message
{
  @autoreleasepool
  {
    NSString *header = @"[OnDebug] ";
    [self.delegate performSelectorOnMainThread: @selector(sendMessage:) withObject:[NSString stringWithFormat:@"%@ %@\n", header, message]  waitUntilDone:NO];
  }
}

@end
