//
//  SyncNowManager.h
//  ReactNativeKantar
//
//  Created by Elie Slama on 09/12/15.
//  Copyright © 2015 Vox Populi. All rights reserved.
//

#ifndef SyncNowManager_h
#define SyncNowManager_h

#endif /* SyncNowManager_h */

const int kBufferSize       = 4096;
const int kSampleRate       = 24000;
const int kBitsPerChannel   = 16;
const int kNumChannel       = 1;

// Base Libs

#import <UIKit/UIKit.h>
#include <AVFoundation/AVAudioSession.h>

// SyncNow

#import "AudioCapturer.h"
#import "AudioDetector.h"

// React

#import   "RCTBridgeModule.h"
#import   "RCTLog.h"

@interface SyncNowManager : NSObject <RCTBridgeModule, AudioDetectorDelegate, AudioCaptureDelegate>
{
  NSString    *libVersion;
  BOOL        mustBeReinit;
  AudioCapturer*      audioCapturer;
  AudioDetector*      audioDetector;
}

- (void) sendDataToJs: (NSObject *)data error:(bool)error message:(NSString *)message;

@end
