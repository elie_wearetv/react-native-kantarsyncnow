/**
 * Copyright (c) 2014 Kantar Media. All rights reserved.
 *
 * This source code and any compilation or derivative thereof is the proprietary
 * information of Kantar Media. and is confidential in nature.
 *
 * Under no circumstances is this software to be combined with any Open Source
 * Software in any way or placed under an Open Source License of any type without
 * the express written permission of Kantar Media.
 *
 * Platform   : ios
 * Created    : 17/01/2014
 * Author     : lraulet
 *
 * AudioCapturer.mm
 */

#include "AudioCapturer.h"
#import <AVFoundation/AVAudioSession.h>

AudioCapturer::AudioCapturer(id<AudioCaptureDelegate> delegate, AudioCaptureConfiguration& configuration)
{
  mIsRunning = FALSE;
  mDelegate = delegate;
  mAudioCaptureConfig.sampleRate = configuration.sampleRate;
  mAudioCaptureConfig.bitsPerChannel = configuration.bitsPerChannel;
  mAudioCaptureConfig.bufferSize = configuration.bufferSize;
  mAudioCaptureConfig.numChannel = configuration.numChannel;
  
  try
  {
    SetupAudio();
  }
  catch (CAXException &e) {
    char buf[256];
    fprintf(stderr, "Error: %s (%s)\n", e.mOperation, e.FormatError(buf));
  }
  catch (...) {
    fprintf(stderr, "An unknown error occurred\n");
  }
}

AudioCapturer::~AudioCapturer()
{
  AudioQueueDispose(mQueue, TRUE);
}


//Call-back function: called when an audio buffer is available
void AudioCapturer::MyInputBufferHandler(void *								inUserData,
                                         AudioQueueRef						inAQ,
                                         AudioQueueBufferRef					inBuffer,
                                         const AudioTimeStamp *				inStartTime,
                                         UInt32								inNumPackets,
                                         const AudioStreamPacketDescription*	inPacketDesc)
{
  AudioCapturer *aqr = (AudioCapturer *)inUserData;
  try
  {
    if (inNumPackets > 0)
    {
      //Send audio buffer to dection library
      [aqr->mDelegate pushBuffer:inBuffer->mAudioData withBufferSize:aqr->mAudioCaptureConfig.bufferSize];
    }
    // if we're not stopping, re-enqueue the buffer so that it gets filled again
    if (aqr->IsRunning())
    {
      XThrowIfError(AudioQueueEnqueueBuffer(inAQ, inBuffer, 0, NULL), "AudioQueueEnqueueBuffer failed");
    }
  }
  catch (CAXException e)
  {
    char buf[256];
    fprintf(stderr, "Error: %s (%s)\n", e.mOperation, e.FormatError(buf));
  }
}

Boolean AudioCapturer::StartCapture()
{
  try
  {
    //Init the AudioQueue with audio record properties and call-back function
    XThrowIfError(AudioQueueNewInput(&mRecordFormat, MyInputBufferHandler,
                                     this, NULL, NULL, 0, &mQueue), "AudioQueueNewInput failed");
    // get the record format back from the queue's audio converter
    UInt32 size = sizeof(mRecordFormat);
    XThrowIfError(AudioQueueGetProperty(mQueue, kAudioQueueProperty_StreamDescription,
                                        &mRecordFormat, &size), "couldn't get queue's format");
    // allocate and enqueue buffers
    for (int i = 0; i < kNumberRecordBuffers; ++i) {
      XThrowIfError(AudioQueueAllocateBuffer(mQueue, mAudioCaptureConfig.bufferSize, &mBuffers[i]),
                    "AudioQueueAllocateBuffer failed");
      XThrowIfError(AudioQueueEnqueueBuffer(mQueue, mBuffers[i], 0, NULL),
                    "AudioQueueEnqueueBuffer failed");
    }
    // start the queue
    mIsRunning = true;
    XThrowIfError(AudioQueueStart(mQueue, NULL), "AudioQueueStart failed");
  }
  catch (CAXException &e) {
    char buf[256];
    fprintf(stderr, "Error: %s (%s)\n", e.mOperation, e.FormatError(buf));
    return FALSE;
  }
  catch (...) {
    fprintf(stderr, "An unknown error occurred\n");
    return FALSE;
  }
  return  TRUE;
}

Boolean AudioCapturer::StopCapture()
{
  mIsRunning=FALSE;
  try
  {   //Stop AudioQueue process
    XThrowIfError(AudioQueueStop(mQueue, true), "AudioQueueStop failed");
  }
  catch (CAXException &e) {
    char buf[256];
    fprintf(stderr, "Error: %s (%s)\n", e.mOperation, e.FormatError(buf));
    return FALSE;
  }
  catch (...) {
    fprintf(stderr, "An unknown error occurred\n");
    return FALSE;
  }
  //Free AudioQueue allocations
  AudioQueueDispose(mQueue, true);
  return TRUE;
}

void AudioCapturer::SetupAudio()
{
  //Audio record properties setup
  
  memset(&mRecordFormat, 0, sizeof(mRecordFormat));
  //Setup some record properties according AudioSession properties
  AVAudioSession *audioSession = [AVAudioSession sharedInstance];
  
  NSError *outError = nil;
  bool okState = false;
  okState= [audioSession setPreferredSampleRate:mAudioCaptureConfig.sampleRate error:&outError];
  if( (okState != true) || (outError!= nil) )
  {
    NSLog(@"Unable to set preferred sample rate\n");
  }
  okState= [audioSession setPreferredInputNumberOfChannels:mAudioCaptureConfig.numChannel error:&outError];
  if( (okState != true) || (outError!= nil) )
  {
    NSLog(@"Unable to set preferred number of channel\n");
  }
  
  mAudioCaptureConfig.sampleRate = [audioSession sampleRate];
  mAudioCaptureConfig.numChannel = (int)[audioSession inputNumberOfChannels];
  
  
  mRecordFormat.mSampleRate = [audioSession sampleRate];
  mRecordFormat.mChannelsPerFrame = (UInt32)[audioSession inputNumberOfChannels];
  mRecordFormat.mFormatID = kAudioFormatLinearPCM;
  mRecordFormat.mFormatFlags = kLinearPCMFormatFlagIsSignedInteger | kLinearPCMFormatFlagIsPacked;
  mRecordFormat.mBitsPerChannel = mAudioCaptureConfig.bitsPerChannel;
  mRecordFormat.mBytesPerPacket = mRecordFormat.mBytesPerFrame = (mRecordFormat.mBitsPerChannel / 8) * mRecordFormat.mChannelsPerFrame;
  mRecordFormat.mFramesPerPacket = mAudioCaptureConfig.numChannel;
}

AudioCaptureConfiguration* AudioCapturer::GetAudioCaptureConfiguration()
{
  return &mAudioCaptureConfig;
}




