//
//  SyncNowManager.m
//  ReactNativeKantar
//
//  Created by Elie Slama on 09/12/15.
//  Copyright © 2015 Vox Populi. All rights reserved.
//

#import "SyncNowManager.h"
#import "RCTEventDispatcher.h"
#include <AudioToolbox/AudioToolbox.h>
#include "AudioCaptureConfiguration.h"

@interface SyncNowManager()
{
  NSString *license;
  int payloadID;
  int timestamp;
}

@end

@implementation SyncNowManager

@synthesize bridge = _bridge;

// Initialize the module

- (void) sendDataToJs: (NSObject *)data error:(bool)error message:(NSString *)message  {
  [self.bridge.eventDispatcher
    sendAppEventWithName:@"SyncNowEvent"
    body:@{@"data": data, @"error": @(error), @"message": message}];
}

- (void) sendError: (NSError *)err {
  [self sendDataToJs:@{} error:true message:err.localizedDescription];
}

- (void) sendInfo: (NSObject *)data {
  [self sendDataToJs:data error:false message:@""];
}

- (void) sendMessage:(NSString *)text
{
  [self sendDataToJs:@{} error:NO message:text];
}



RCT_EXPORT_METHOD(initSyncNow:(NSString *)myLicense
                  identifierBits:(nonnull NSNumber *)identifierBits
                  timestampBits:(nonnull NSNumber *)timestampBits)
{
  NSObject *data = @{@"licence": myLicense,
                     @"identifierBits": identifierBits ,
                     @"timestampBits": timestampBits,};
  
  
  [self sendDataToJs:data error:false message:@"Init OK"];
  
  license = myLicense;
  payloadID = [identifierBits intValue];
  timestamp = [timestampBits intValue];
  
  [self initSession];
  
}

RCT_EXPORT_MODULE();

- (void) initSession {

  [self audioSessionManagement];
  
  // Create the Recorder
  struct AudioCaptureConfiguration config;
  config.bitsPerChannel= kBitsPerChannel;
  config.bufferSize = kBufferSize;
  config.numChannel = kNumChannel;
  config.sampleRate = kSampleRate;
  
  audioCapturer = new AudioCapturer(self, config);
  
  [self initAudioDetector];
  
  
  if(![audioDetector startReader:NO])
  {
    [self sendMessage:@"Detector was not started, the sample didn't initialize correctly!"];
  }
  else
  {
    if(!audioCapturer->StartCapture())
    {
      [self sendMessage:@"Recorder was not started!"];
    }
  }
}

-(void) initAudioDetector
{
  // Setting management is particular with ios, default options are not taken into account.
  // Values must be set manually by the user in the 'Settings' item of device to be seen by the NSUserDefaults class otherwise the returned value is empty.
  // That's why default values are also set into the code
  BOOL timestampLoop = YES;
  BOOL Log           = NO;
  
  //Create reader config from settings
  AudioDetectorConfig* audioDetectorConfig = [[AudioDetectorConfig alloc]init];
  [audioDetectorConfig setTimeStampLoop:timestampLoop];
  [audioDetectorConfig setLicense:license];
  [audioDetectorConfig setLog:Log];
  [audioDetectorConfig setPayloadIdNumberOfBits:payloadID];
  [audioDetectorConfig setTimeStampNumberOfBits:timestamp];
  
  // use audio capture configuration from the audioCapture itself that might have been updated during initialization
  audioDetector = [[AudioDetector alloc] initWithUI:*audioCapturer->GetAudioCaptureConfiguration() audioDetectionConfig:audioDetectorConfig];
  audioDetector.delegate = self;
  
}


#pragma mark - Notification management
- (void) audioInterruption:(NSNotification*)notification
{
  NSDictionary *interruptionDict = notification.userInfo;
  NSNumber* interruptionTypeValue = [interruptionDict valueForKey:AVAudioSessionInterruptionTypeKey];
  NSNumber* interruptionOptionValue = [interruptionDict valueForKey:AVAudioSessionInterruptionOptionKey];
  NSUInteger interruptionType = [interruptionTypeValue intValue];
  NSUInteger interruptionOption = [interruptionOptionValue intValue];
  
  if (interruptionType == AVAudioSessionInterruptionTypeBegan)
  {
    NSLog(@"interruption begin");
    audioCapturer->StopCapture();
    [audioDetector stopReader];
  }
  if (interruptionType == AVAudioSessionInterruptionTypeEnded)
  {
    NSLog(@"interruption end");
    //Interruption end is send two time, with the first time with option 'ShouldResume'
    // the second time option is not set.
    //As reader must be rerun one time, only the first end interruption is taken into account.
    if (interruptionOption == AVAudioSessionInterruptionOptionShouldResume)
    {
      [audioDetector startReader:NO];
      audioCapturer->StartCapture();
    }
  }
}


- (void) reinitDetector
{
  [self initAudioDetector];
}


#pragma mark - Audio session management

- (void) audioSessionManagement
{
  NSError *activationError = nil;
  AVAudioSession *audioSession = [AVAudioSession sharedInstance];
  [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(audioInterruption:) name:AVAudioSessionInterruptionNotification object:nil];
  if ([audioSession setCategory:AVAudioSessionCategoryPlayAndRecord withOptions:AVAudioSessionCategoryOptionMixWithOthers error:&activationError])
  {
    if (![audioSession setActive:YES error:&activationError])
      NSLog(@"%@", [activationError localizedDescription]);
  }
  else
    NSLog(@"%@", [activationError localizedDescription]);
}


- (void) stopCaptureAndRefreshDisplay
{
  audioCapturer->StopCapture();
  if (mustBeReinit)
    [self reinitDetector];
}

- (void) startCaptureAndRefreshDisplay
{
  audioCapturer->StartCapture();
}

- (void) detect {
  if(audioDetector.isRunning)
  {
    [self stopCaptureAndRefreshDisplay];
    [audioDetector stopReader];
  }
  else
  {
    [audioDetector startReader:NO];
    if(audioDetector.isRunning)
    {
      [self startCaptureAndRefreshDisplay];
    }
    else
    {
      [self sendMessage:@"Audio detector was not started"];
    }
  }
}

- (void)detectAndRecord {
  if(audioDetector.isRunning)
  {
    [self stopCaptureAndRefreshDisplay];
    [audioDetector stopReader];
  }
  else
  {
    [audioDetector startReader: YES];
    if(audioDetector.isRunning)
    {
      [self startCaptureAndRefreshDisplay];
    }
    else
    {
      [self sendMessage:@"Audio detector was not started"];
    }
  }
}

// [audioDetector getWmkPresence]

#pragma mark - Recorder delegate when buffer ready to be pushed
- (int) pushBuffer:(void*) buffer withBufferSize:(NSInteger)size
{
  [audioDetector readBuffer:buffer WithBufferSize:size];
  return 0;
}

@end