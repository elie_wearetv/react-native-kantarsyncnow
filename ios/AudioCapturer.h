/**
 * Copyright (c) 2014 Kantar Media. All rights reserved.
 *
 * This source code and any compilation or derivative thereof is the proprietary
 * information of Kantar Media. and is confidential in nature.
 *
 * Under no circumstances is this software to be combined with any Open Source
 * Software in any way or placed under an Open Source License of any type without
 * the express written permission of Kantar Media.
 *
 * Platform   : ios
 * Created    : 17/01/2014
 * Author     : lraulet
 *
 * AudioCapturer.h
 */

#import <AudioToolbox/AudioToolbox.h>
#import <Foundation/Foundation.h>
#include "CAStreamBasicDescription.h"
#include "CAXException.h"
#define kNumberRecordBuffers	3
#include "AudioCaptureConfiguration.h"

@protocol AudioCaptureDelegate
- (int) pushBuffer:(void*) buffer withBufferSize:(NSInteger) size;

@end

class AudioCapturer
{
public:
  AudioCapturer(id<AudioCaptureDelegate> delegate, AudioCaptureConfiguration& configuration);
  ~AudioCapturer();
  Boolean         StartCapture();
  Boolean         StopCapture();
  Boolean         IsRunning() const			{ return mIsRunning; }
  AudioCaptureConfiguration* GetAudioCaptureConfiguration();
  
  static void MyInputBufferHandler(void *									inUserData,
                                   AudioQueueRef							inAQ,
                                   AudioQueueBufferRef					inBuffer,
                                   const AudioTimeStamp *					inStartTime,
                                   UInt32									inNumPackets,
                                   const AudioStreamPacketDescription*	inPacketDesc);
  
private:
  Boolean						mIsRunning;
  CAStreamBasicDescription	mRecordFormat;
  id<AudioCaptureDelegate>    mDelegate;
  struct AudioCaptureConfiguration   mAudioCaptureConfig;
  
  AudioQueueRef				mQueue;
  AudioQueueBufferRef			mBuffers[kNumberRecordBuffers];
  
  void SetupAudio();
};